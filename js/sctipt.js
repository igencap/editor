const canvas = new fabric.Canvas('canvas', {
    width: 1000,
    height: 500,
});

let deleteIcon = "data:image/svg+xml,%3C%3Fxml version='1.0' encoding='utf-8'%3F%3E%3C!DOCTYPE svg PUBLIC '-//W3C//DTD SVG 1.1//EN' 'http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd'%3E%3Csvg version='1.1' id='Ebene_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' width='595.275px' height='595.275px' viewBox='200 215 230 470' xml:space='preserve'%3E%3Ccircle style='fill:%23F44336;' cx='299.76' cy='439.067' r='218.516'/%3E%3Cg%3E%3Crect x='267.162' y='307.978' transform='matrix(0.7071 -0.7071 0.7071 0.7071 -222.6202 340.6915)' style='fill:white;' width='65.545' height='262.18'/%3E%3Crect x='266.988' y='308.153' transform='matrix(0.7071 0.7071 -0.7071 0.7071 398.3889 -83.3116)' style='fill:white;' width='65.544' height='262.179'/%3E%3C/g%3E%3C/svg%3E";
let cloneIcon = "data:image/svg+xml,%3C%3Fxml version='1.0' encoding='iso-8859-1'%3F%3E%3Csvg version='1.1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' viewBox='0 0 55.699 55.699' width='100px' height='100px' xml:space='preserve'%3E%3Cpath style='fill:%23010002;' d='M51.51,18.001c-0.006-0.085-0.022-0.167-0.05-0.248c-0.012-0.034-0.02-0.067-0.035-0.1 c-0.049-0.106-0.109-0.206-0.194-0.291v-0.001l0,0c0,0-0.001-0.001-0.001-0.002L34.161,0.293c-0.086-0.087-0.188-0.148-0.295-0.197 c-0.027-0.013-0.057-0.02-0.086-0.03c-0.086-0.029-0.174-0.048-0.265-0.053C33.494,0.011,33.475,0,33.453,0H22.177 c-3.678,0-6.669,2.992-6.669,6.67v1.674h-4.663c-3.678,0-6.67,2.992-6.67,6.67V49.03c0,3.678,2.992,6.669,6.67,6.669h22.677 c3.677,0,6.669-2.991,6.669-6.669v-1.675h4.664c3.678,0,6.669-2.991,6.669-6.669V18.069C51.524,18.045,51.512,18.025,51.51,18.001z M34.454,3.414l13.655,13.655h-8.985c-2.575,0-4.67-2.095-4.67-4.67V3.414z M38.191,49.029c0,2.574-2.095,4.669-4.669,4.669H10.845 c-2.575,0-4.67-2.095-4.67-4.669V15.014c0-2.575,2.095-4.67,4.67-4.67h5.663h4.614v10.399c0,3.678,2.991,6.669,6.668,6.669h10.4 v18.942L38.191,49.029L38.191,49.029z M36.777,25.412h-8.986c-2.574,0-4.668-2.094-4.668-4.669v-8.985L36.777,25.412z M44.855,45.355h-4.664V26.412c0-0.023-0.012-0.044-0.014-0.067c-0.006-0.085-0.021-0.167-0.049-0.249 c-0.012-0.033-0.021-0.066-0.036-0.1c-0.048-0.105-0.109-0.205-0.194-0.29l0,0l0,0c0-0.001-0.001-0.002-0.001-0.002L22.829,8.637 c-0.087-0.086-0.188-0.147-0.295-0.196c-0.029-0.013-0.058-0.021-0.088-0.031c-0.086-0.03-0.172-0.048-0.263-0.053 c-0.021-0.002-0.04-0.013-0.062-0.013h-4.614V6.67c0-2.575,2.095-4.67,4.669-4.67h10.277v10.4c0,3.678,2.992,6.67,6.67,6.67h10.399 v21.616C49.524,43.26,47.429,45.355,44.855,45.355z'/%3E%3C/svg%3E%0A";

let deleteImg = document.createElement('img');
deleteImg.src = deleteIcon;


let cloneImg = document.createElement('img');
cloneImg.src = cloneIcon;


fabric.Object.prototype.cornerStyle = 'circle';


/****************************
****************************

    ADDING TEXT

****************************
****************************/

function AddText() {
    let text = new fabric.IText('Text', {
        left: this.canvas.width / 2,
        top: this.canvas.height / 2,
        fill: '#000',
        fontFamily: 'sans-serif',
        hasRotatingPoint: false,
        centerTransform: true,
        originX: 'center',
        originY: 'center',
        lockUniScaling: true
    });

    canvas.add(text);
    canvas.setActiveObject(text);
}

/****************************
****************************

    ADDING RECTANGLE

****************************
****************************/

function AddRectangle() {
    let rect = new fabric.Rect({
        left: this.canvas.width / 2,
        top: this.canvas.height / 2,
        fill: '#22325C',
        width: 100,
        height: 100,
        transparentCorners: true,
        objectCaching: false,
    });

    canvas.add(rect);
    canvas.setActiveObject(rect);
}

/****************************
****************************

    ADDING CIRCLE

****************************
****************************/

function AddCircle() {
    let circ = new fabric.Circle({
        left: this.canvas.width / 2,
        top: this.canvas.height / 2,
        radius: 50,
        originX: 'center',
        originY: 'center',
        strokeWidth: 0,
        fill: '#A2A39C',
        objectCaching: false,
    });

    canvas.add(circ);
    canvas.setActiveObject(circ);
}

/****************************
****************************

    ADDING TRIANGLE

****************************
****************************/

function AddTriangle() {
    let triangle = new fabric.Triangle({
        left: this.canvas.width / 2,
        top: this.canvas.height / 2,
        fill: '#78909c',
        width: 100,
        height: 100,
        originX: 'center',
        originY: 'center',
        strokeWidth: 0
    })

    canvas.add(triangle);
    canvas.setActiveObject(triangle);
}

/****************************
****************************

    ADDING LINE

****************************
****************************/

function AddLine() {
    const line = new fabric.Line([50, 350, 250, 350], {
        stroke: colorPicker.value,
        strokeWidth: 2
    });
    canvas.add(line);
}

/****************************
****************************

    DRAWING MODE

****************************
****************************/

let isDrawing = false;
let lines = [];
let currentLine;
let firstClickCoordinates = null;
let drawingEnter = document.getElementById('startDrawing');
let drawingQuit = document.getElementById('quitDrawing');
let drawingStatus = document.getElementById('status');

function drawingOn() {
    isDrawing = true;
    firstClickCoordinates = null; // Reset first click coordinates

    if (isDrawing === true) {
        drawingStatus.textContent = "Drawing Mode: On"
    }
}

function drawingOff() {
    isDrawing = false;
    if (currentLine) {
        canvas.remove(currentLine);
        lines.pop(currentLine);
        currentLine = null;
    }

    firstClickCoordinates = null
    pathData = null
    mergedShape = null
    if (lines) {
        lines.forEach(function (line) {
            canvas.remove(line);
        });
    }

    if (isDrawing === false) {
        drawingStatus.textContent = "Drawing Mode: Off"
    }
}

drawingEnter.addEventListener('click', drawingOn);
drawingQuit.addEventListener('click', drawingOff);

// ... Previous code ...

canvas.on('mouse:down', function (event) {
    if (isDrawing) {
        let pointer = canvas.getPointer(event.e);

        if (!currentLine || !firstClickCoordinates) {
            // Start a new shape
            firstClickCoordinates = [pointer.x, pointer.y];
            currentLine = new fabric.Line([pointer.x, pointer.y, pointer.x, pointer.y], {
                stroke: colorPicker.value,
                strokeWidth: 2,
                selectable: false
            });
            canvas.add(currentLine);
            lines.push(currentLine);
        } else {
            // Continue the current shape
            let endPoint = canvas.getPointer(event.e);
            currentLine.set({ x2: endPoint.x, y2: endPoint.y });

            // Check if the current shape reaches the vicinity of the first click
            let threshold = 10; // Adjust this threshold as needed
            let dx = Math.abs(firstClickCoordinates[0] - endPoint.x);
            let dy = Math.abs(firstClickCoordinates[1] - endPoint.y);
            if (dx <= threshold && dy <= threshold) {
                // Stop drawing and finalize the shape
                var pathData = lines.reduce(function (acc, line) {
                    return acc.concat('L', line.x2, line.y2);
                }, ['M', lines[0].x1, lines[0].y1]).join(' ');
                var mergedShape = new fabric.Path(pathData, {
                    stroke: lines[0].stroke,
                    strokeWidth: lines[0].strokeWidth,
                    fill: colorPicker.value,
                    selectable: true
                });
                canvas.add(mergedShape);

                // Remove the lines
                lines.forEach(function (line) {
                    canvas.remove(line);
                });
                lines = [];

                // Update canvas
                canvas.renderAll();

                // Reset first click coordinates for the next shape
                firstClickCoordinates = null;

                // Stop the drawing mode after completing the shape
                drawingOff();
            } else {
                // Create a new line for the current shape
                currentLine = new fabric.Line([endPoint.x, endPoint.y, endPoint.x, endPoint.y], {
                    stroke: colorPicker.value,
                    strokeWidth: 2,
                    selectable: false
                });
                canvas.add(currentLine);
                lines.push(currentLine);
            }
        }
    }
});

// ... Rest of the code ...


canvas.on('mouse:move', function (event) {
    if (isDrawing && currentLine) {
        let pointer = canvas.getPointer(event.e);
        currentLine.set({ x2: pointer.x, y2: pointer.y });
        canvas.renderAll();
    }
});




console.log(isDrawing)

/****************************
****************************

    ADDING BACKGROUND IMAGE

****************************
****************************/
document.getElementById('addImage').addEventListener("change", function (e) {
    var file = e.target.files[0];
    var reader = new FileReader();
    reader.onload = function (f) {
        var data = f.target.result;
        fabric.Image.fromURL(data, function (img) {
            // add background image
            canvas.setBackgroundImage(img, canvas.renderAll.bind(canvas), {
                scaleX: canvas.width / img.width,
                scaleY: canvas.height / img.height
            });
        });
    };
    reader.readAsDataURL(file);
});


/****************************
****************************

    UNDO DELETED

****************************
****************************/
// Array to store deleted objects
let deletedObjects = [];

// Event listener for the Undo button
document.getElementById('undoButton').addEventListener('click', undoDelete);

// Event listener for object removal
canvas.on('object:removed', function (event) {
    const removedObject = event.target;
    deletedObjects.push(removedObject);
});

// Function to undo the last delete operation
function undoDelete() {
    if (deletedObjects.length > 0) {
        const lastDeletedObject = deletedObjects.pop();
        canvas.add(lastDeletedObject);
        canvas.renderAll();
    }
}


/****************************
****************************

    DELETE CONTROL

****************************
****************************/
fabric.Object.prototype.controls.deleteControl = new fabric.Control({
    x: 0.5,
    y: -0.5,
    offsetY: 16,
    cursorStyle: 'pointer',
    mouseUpHandler: deleteObject,
    render: renderIcon(deleteImg),
    cornerSize: 24
});

/****************************
****************************

    CLONE CONTROL

****************************
****************************/

fabric.Object.prototype.controls.clone = new fabric.Control({
    x: -0.5,
    y: -0.5,
    offsetY: -16,
    offsetX: -16,
    cursorStyle: 'pointer',
    mouseUpHandler: cloneObject,
    render: renderIcon(cloneImg),
    cornerSize: 24
});

function deleteObject(eventData, transform) {
    let target = transform.target;
    let canvas = target.canvas;
    canvas.remove(target);
    canvas.requestRenderAll();
}

function cloneObject(eventData, transform) {
    let target = transform.target;
    let canvas = target.canvas;
    target.clone(function (cloned) {
        cloned.left += 10;
        cloned.top += 10;
        canvas.add(cloned);
    });
}


function renderIcon(icon) {
    return function renderIcon(ctx, left, top, styleOverride, fabricObject) {
        let size = this.cornerSize;
        ctx.save();
        ctx.translate(left, top);
        ctx.rotate(fabric.util.degreesToRadians(fabricObject.angle));
        ctx.drawImage(icon, -size / 2, -size / 2, size, size);
        ctx.restore();
    }
}



function clearCanvas() {
    canvas.clear();
}

/****************************
****************************

    MODAL
    
****************************
****************************/

// Creating a modal container
const modalContainer = document.createElement('div');
modalContainer.classList.add('modal-container');
modalContainer.style.display = 'none';
document.body.appendChild(modalContainer);

let costQuadrature = 300;
let area = 40;
let costRent = 12000;

// Function to create and show the modal with shape information
function showModal(shapeInfo) {
    modalContainer.innerHTML = ''; // Clear previous content
    const modalContent = document.createElement('div');
    modalContent.classList.add('modal-content');

    const modalTitle = document.createElement('h3');
    modalTitle.textContent = 'Название места';
    modalContent.appendChild(modalTitle);


    // Inner container
    const modalInnerContainer = document.createElement('div');
    modalInnerContainer.classList.add('modal-inner-container');
    modalContent.appendChild(modalInnerContainer);


    // Inner item
    const modalInnerItem = document.createElement('div');
    modalInnerItem.classList.add('modal-inner-item');
    modalInnerContainer.appendChild(modalInnerItem);

    const modalInnerItem2 = document.createElement('div');
    modalInnerItem2.classList.add('modal-inner-item');
    modalInnerContainer.appendChild(modalInnerItem2);

    const modalInnerItem3 = document.createElement('div');
    modalInnerItem3.classList.add('modal-inner-item');
    modalInnerContainer.appendChild(modalInnerItem3);

    const modalInnerItem4 = document.createElement('div');
    modalInnerItem4.classList.add('modal-inner-item');
    modalInnerItem4.classList.add('modal-inner-items');
    modalInnerContainer.appendChild(modalInnerItem4);

    // Inner info
    const modalInnerInfoTitle = document.createElement('h4');
    modalInnerInfoTitle.classList.add('modal-inner-info-title');
    modalInnerInfoTitle.textContent = 'Статус';

    const modalInnerInfoTitle2 = document.createElement('h4');
    modalInnerInfoTitle2.classList.add('modal-inner-info-title');
    modalInnerInfoTitle2.textContent = 'Тип';

    const modalInnerInfoTitle3 = document.createElement('h4');
    modalInnerInfoTitle3.classList.add('modal-inner-info-title');
    modalInnerInfoTitle3.innerHTML = 'Стоимость' + ' м<sup>2</sup>';

    const modalInnerInfoTitle4 = document.createElement('h4');
    modalInnerInfoTitle4.classList.add('modal-inner-info-title');
    modalInnerInfoTitle4.textContent = 'Площадь';

    const modalInnerInfoTitle5 = document.createElement('h4');
    modalInnerInfoTitle5.classList.add('modal-inner-info-title');
    modalInnerInfoTitle5.textContent = 'Стоимость аренды';


    modalInnerItem.appendChild(modalInnerInfoTitle);
    modalInnerItem2.appendChild(modalInnerInfoTitle2);
    modalInnerItem3.appendChild(modalInnerInfoTitle3);
    modalInnerItem4.appendChild(modalInnerInfoTitle4);
    modalInnerItem4.appendChild(modalInnerInfoTitle5);

    const modalInnerInfoText = document.createElement('span');
    modalInnerInfoText.classList.add('modal-inner-info-text');
    modalInnerInfoText.classList.add('booked'); //  It can be booked
    modalInnerInfoText.textContent = 'Свободно';

    const modalInnerInfoText2 = document.createElement('span');
    modalInnerInfoText2.classList.add('modal-inner-info-text');
    modalInnerInfoText2.textContent = 'Свободного назначения';

    const modalInnerInfoText3 = document.createElement('span');
    modalInnerInfoText3.classList.add('modal-inner-info-text');
    modalInnerInfoText3.textContent = `${costQuadrature}` + ' ₽';


    const modalInnerInfoText4 = document.createElement('span');
    modalInnerInfoText4.classList.add('modal-inner-info-text');
    modalInnerInfoText4.innerHTML = `${area}` + 'м<sup>2</sup> = ';

    const modalInnerInfoText5 = document.createElement('span');
    modalInnerInfoText5.classList.add('modal-inner-info-text');
    modalInnerInfoText5.innerHTML = `${costRent}` + ' ₽ в месяц';


    modalInnerItem.appendChild(modalInnerInfoText);
    modalInnerItem2.appendChild(modalInnerInfoText2);
    modalInnerItem3.appendChild(modalInnerInfoText3);
    modalInnerItem4.appendChild(modalInnerInfoText4);
    modalInnerItem4.appendChild(modalInnerInfoText5);


    // Close modal
    const closeModal = document.createElement('button');
    closeModal.textContent = '';
    closeModal.classList.add('modal-close');
    closeModal.addEventListener('click', hideModal);
    modalContent.appendChild(closeModal);

    modalContainer.appendChild(modalContent);
    modalContainer.style.display = 'flex';
}

// Function to hide the modal
function hideModal() {
    modalContainer.style.display = 'none';
}

// Event listener to handle clicks on the canvas
canvas.on('mouse:up', function (event) {
    if (event.target) {
        // If the click is on a shape, show the modal with its information
        showModal();
    } else {
        // If the click is not on a shape, hide the modal
        hideModal();
    }
});




/****************************
****************************

    COLOR PICKER

****************************
****************************/
// Color picker event listener
const colorPicker = document.getElementById('colorPicker');
colorPicker.addEventListener('input', function () {
    const selectedColor = this.value;

    // Change the color of the selected shape
    const activeObject = canvas.getActiveObject();
    if (activeObject) {
        activeObject.set('fill', selectedColor);
        activeObject.set('stroke', selectedColor);
        canvas.renderAll();
    }
});

canvas.renderAll();